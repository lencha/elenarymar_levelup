package ru.levelup.elena.rymar.qa.homework_09;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListPostsResponse {
    @SerializedName("_meta")
    private Meta meta;
    @SerializedName("result")
    private List<Post> post;

    public ListPostsResponse(Meta meta, List<Post> post) {
        this.meta = meta;
        this.post = post;
    }

    public Meta getMeta() {
        return meta;
    }

    public List<Post> getPost() {
        return post;
    }

    @Override
    public String toString() {
        return "ListUsersResponse{" +
                "_meta=" + meta +
                ", results=" + post +
                '}';
    }
}
