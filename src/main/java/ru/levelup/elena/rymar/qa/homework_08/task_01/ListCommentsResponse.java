package ru.levelup.elena.rymar.qa.homework_08.task_01;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListCommentsResponse {
    @SerializedName("_meta")
    private Meta meta;
    @SerializedName("result")
    private List<Comment> comment;

    public ListCommentsResponse(Meta meta, List<Comment> comment) {
        this.meta = meta;
        this.comment = comment;
    }

    public Meta getMeta() {
        return meta;
    }

    public List<Comment> getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return "ListCommentsResponse{" +
                "_meta=" + meta +
                ", comment=" + comment +
                '}';
    }
}
