package ru.levelup.elena.rymar.qa.homework_09;

import com.google.gson.annotations.SerializedName;

public class PostDetailsResponse {
    @SerializedName("_meta")
    private Meta meta;
    private Post result;

    public PostDetailsResponse(Meta meta, Post result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public Post getResult() {
        return result;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "UserDetailsResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }

    static class Builder {

        private Meta meta;
        private Post result;

        public Builder() {
        }

        public Builder meta(Meta meta) {
            this.meta = meta;
            return this;
        }

        public Builder result(Post result) {
            this.result = result;
            return this;
        }

        public PostDetailsResponse build() {
            return new PostDetailsResponse(meta, result);
        }
    }
}
