package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class InboxPage extends BasePage{

    @FindBy(className = "portal-menu-element__text")
    private WebElement menu;

    @FindBy(xpath = "//*[@class='letter__author']/span")
    private WebElement mailAddress;

    @FindBy(xpath = "//*[@class='thread__header']//h2")
    private  WebElement emailSubject;

    @FindBy(xpath = "//*[@class='ll-sj__normal']")
    private List<WebElement> mailSubjectsList;

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public String getTextMenu() {
        return wait.until(visibilityOf(menu)).getText();
    }

    public String getMailAddress() {
        return wait.until(ExpectedConditions.visibilityOf(mailAddress)).getAttribute("title");
    }

    public void openMail(int index) {
        mailSubjectsList.get(index).click();
    }

    public String getMailBodyTextContains(String partialText) {
        return wait.until(visibilityOfElementLocated(
                By.xpath((String.format("//*[text()[contains(., '%s')]]", partialText))))).getText();
    }
}
