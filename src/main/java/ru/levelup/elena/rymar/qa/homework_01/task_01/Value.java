package ru.levelup.elena.rymar.qa.homework_01.task_01;

import java.util.Scanner;

public class Value{

    Scanner scanner = new Scanner(System.in);

    public Value() {
        this.scanner = scanner;
    }

    public int getIntValue(){
        int num=0;
        System.out.println("Введите число:");
        if (scanner.hasNextInt()){
            num = scanner.nextInt();
        }else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();//рекурсия
            num = getIntValue();
        }
        return num;
    }
}
