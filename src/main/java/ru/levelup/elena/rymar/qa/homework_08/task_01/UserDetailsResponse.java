package ru.levelup.elena.rymar.qa.homework_08.task_01;

import com.google.gson.annotations.SerializedName;

public class UserDetailsResponse {
    @SerializedName("_meta")
    private Meta meta;
    private People result;

    public UserDetailsResponse(Meta meta, People result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public People getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "UserDetailsResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }
}
