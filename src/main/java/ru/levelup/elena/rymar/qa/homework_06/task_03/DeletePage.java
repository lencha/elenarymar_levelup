package ru.levelup.elena.rymar.qa.homework_06.task_03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelup.elena.rymar.qa.homework_06.task_01.BasePage;

public class DeletePage extends BasePage {

    @FindBy(xpath = "//a[contains(@href, \"/trash/\")]")
    private WebElement deleteFolder;

    public DeletePage(WebDriver driver) {
        super(driver);
    }

    public void pressDeleteFolderButton(){
        wait.until(ExpectedConditions.elementToBeClickable(deleteFolder));
        deleteFolder.click();
    }

}
