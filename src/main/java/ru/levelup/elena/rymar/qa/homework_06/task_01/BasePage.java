package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;

    @FindBy(className = "llc__subject")
    protected List<WebElement> mailSubjectList;

    @FindBy(className = "ll-sp__normal")
    protected List<WebElement> mailTextList;

    @FindBy(className = "ll-crpt")
    protected List<WebElement> mailSendAddressList;

    @FindBy(css = "[title='Все папки']")
    protected WebElement allFolders;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public String getMailText(int index) {
        return mailTextList.get(index).getText();
    }

    public String getMailSendAddress(int index) {
        return mailSendAddressList.get(index).getText();
    }

    public String getMailSubject(int index) {
        return mailSubjectList.get(index).getText();
    }

    public void pressAllFolders(){
        wait.until(ExpectedConditions.elementToBeClickable(allFolders));
        allFolders.click();
    }

}
