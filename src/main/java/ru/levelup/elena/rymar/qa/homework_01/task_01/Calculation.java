package ru.levelup.elena.rymar.qa.homework_01.task_01;

public class Calculation {

    double result;
    private static final String WRONG_OPERATION_MESSAGE = "Введена неверная операция. Повторите ввод, выбрав одно из значений: '+', '-', '*', '^', '!', 'f'";

    public double calc(int val1, int val2, char operation){
        Operation o = new Operation();
        Addition sum = new Addition();
        Subtraction sub = new Subtraction();
        Multiplication ml = new Multiplication();
        Exponentiation exp = new Exponentiation();
        Factorial f = new Factorial();
        Fibonacci fi = new Fibonacci();
        switch (operation){
            case '+':
                result = sum.addition(val1, val2);
                break;
            case '-':
                result = sub.subtraction(val1, val2);
                break;
            case '*':
                result = ml.multiplication(val1, val2);
                break;
            case '^':
                result = exp.exponentiation(val1, val2);
                break;
            case '!':
                result = f.factorial(val1);
                break;
            case 'f':
                result = fi.fibonacci(val1);
                break;
            default:
                System.out.println(WRONG_OPERATION_MESSAGE);
                operation = o.getOperation();
                result = calc(val1, val2, operation);    //рекурсия
        }
        return result;
    }
}
