package ru.levelup.elena.rymar.qa.homework_08.task_01;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ListUsersResponse {
    @SerializedName("_meta")
    private Meta meta;
    @SerializedName("result")
    private List<People> people;

    public ListUsersResponse(Meta meta, List<People> people) {
        this.meta = meta;
        this.people = people;
    }

    public Meta getMeta() {
        return meta;
    }

    public List<People> getPeople() {
        return people;
    }

    @Override
    public String toString() {
        return "ListUsersResponse{" +
                "_meta=" + meta +
                ", results=" + people +
                '}';
    }
}
