package ru.levelup.elena.rymar.qa.homework_07.task_01;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MailRuPage {
    @Step("Open page")
    public MailRuPage open() {
        return Selenide.open("/", MailRuPage.class);
    }

    private static SelenideElement usernameTextField = $(By.name("login"));
    private static SelenideElement passwordTextField = $(By.id("mailbox:password"));

    public MailRuPage clickEnterButton() {
        $x("//input[@class='o-control']").click();
        return this;
    }

    @Step("Login as {0} with password {1}")
    public void login(String username, String password) {
        usernameTextField.sendKeys(username);
        clickEnterButton();
        passwordTextField.sendKeys(password);
        clickEnterButton();
    }

}
