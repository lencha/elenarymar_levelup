package ru.levelup.elena.rymar.qa.homework_06.task_02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelup.elena.rymar.qa.homework_06.task_01.BasePage;

public class TestPage extends BasePage {

    @FindBy(className = "sidebar__full")
    private WebElement testFolder;

    public TestPage(WebDriver driver) {
        super(driver);
    }

    public void pressTestFolderButton(){
        wait.until(ExpectedConditions.elementToBeClickable(testFolder));
        wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(testFolder, By.xpath("//div[contains(text(), 'Тест')]")));
        testFolder.click();
    }
}
