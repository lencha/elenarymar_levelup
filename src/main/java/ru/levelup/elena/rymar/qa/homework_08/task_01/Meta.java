package ru.levelup.elena.rymar.qa.homework_08.task_01;

import java.util.Objects;

public class Meta {
    private boolean success;
    private String code;
    private String message;

    public Meta(boolean success, String code) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meta meta = (Meta) o;
        return success == meta.success &&
                Objects.equals(code, meta.code) &&
                Objects.equals(message, meta.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, code, message);
    }

    @Override
    public String toString() {
        return "Meta{" +
                "success=" + success +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
