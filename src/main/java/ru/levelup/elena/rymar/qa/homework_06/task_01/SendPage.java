package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class SendPage extends BasePage{

    @FindBy(css = "[title^='Отправленные']")
    private WebElement sendFolderButton;

    @FindBy(xpath = "//*[@class='llc__item llc__item_correspondent']")
    private List<WebElement> mailSendAddressList;

    public SendPage(WebDriver driver) {
        super(driver);
    }

    public void pressSendFolderButton(){
        wait.until(ExpectedConditions.elementToBeClickable(sendFolderButton));
        sendFolderButton.click();
    }

    public String getMailSendAddress(int index) {
        return mailSendAddressList.get(index).getAttribute("title");
    }

}
