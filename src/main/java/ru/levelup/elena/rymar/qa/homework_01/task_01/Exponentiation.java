package ru.levelup.elena.rymar.qa.homework_01.task_01;

public class Exponentiation {
    public int exponentiation(int a, int b) {
        int result = 1;
        for (int i = 1; i <= b; i++){
            result *= a;
        }
        return result;
    }
}
