package ru.levelup.elena.rymar.qa.homework_08.task_01;

import com.google.gson.annotations.SerializedName;

public class PostDetailsResponse {
    @SerializedName("_meta")
    private Meta meta;
    private Post result;

    public PostDetailsResponse(Meta meta, Post result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public Post getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "UserDetailsResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }
}
