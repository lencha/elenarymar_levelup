package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LetterPage extends BasePage{
    @FindBy(css = "[title='Написать письмо']")
    private WebElement writeMailButton;

    @FindBy(css = "[data-type='to'] input")
    private WebElement to;

    @FindBy(css = "input[name='Subject']")
    private WebElement subject;

    @FindBy(css = "[role='textbox']")
    private WebElement body;

    @FindBy(css = "[title='Сохранить']")
    private WebElement saveDraftButton;

    //@FindBy(css = "[type='button'][title='Закрыть']")
    @FindBy(css = "[title^='Закрыть']")
    private WebElement closeDraftButton;

    @FindBy(xpath = "//*[@title='Отправить']")
    private WebElement sendDraftButton;

    public LetterPage(WebDriver driver) {
        super(driver);
    }

    public void createNewLetter() {
        wait.until(ExpectedConditions.elementToBeClickable(writeMailButton));
        //new Actions(driver).sendKeys("n").perform();
        writeMailButton.click();
    }

    public void setAddress(String address){
        wait.until(ExpectedConditions.visibilityOf(to));
        to.sendKeys(address);
    }

    public void setSubject(String mailSubject){
        wait.until(ExpectedConditions.elementToBeClickable(subject));
        subject.sendKeys(mailSubject);
    }

    public void setBody(String text){
        wait.until(ExpectedConditions.elementToBeClickable(body));
        body.sendKeys(text);
    }

    public void pressSaveDraft(){
        wait.until(ExpectedConditions.elementToBeClickable(saveDraftButton));
        saveDraftButton.click();
    }

    public void pressCloseDraft(){
        wait.until(ExpectedConditions.elementToBeClickable(closeDraftButton));
        closeDraftButton.click();
    }

    public void pressSendDraft(){
        wait.until(ExpectedConditions.elementToBeClickable(sendDraftButton));
        sendDraftButton.click();
    }
}
