package ru.levelup.elena.rymar.qa.homework_01.task_01;

public class Factorial {
    public int factorial(int a) {
        int result = 1;
        for (int i = 1; i <= a; i++) {
            result *= i;
        }
        return result;
    }
}
