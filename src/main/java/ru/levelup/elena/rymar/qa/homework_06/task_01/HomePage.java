package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class HomePage extends BasePage{

    @FindBy(name = "login")
    private WebElement usernameTextField;

    @FindBy(id = "mailbox:password")
    private WebElement passwordTextField;

    @FindBy(xpath = "//input[@class='o-control']")
    private WebElement enterButton;

    @FindBy(id = "PH_logoutLink")
    private WebElement usernameLogout;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void open(String mail) {
        driver.get(mail);
    }

    public void clickEnterButton() {
        enterButton.click();
    }

    public void login(String username, String password) {
        wait.until(visibilityOf(usernameTextField)).sendKeys(username);
        clickEnterButton();
        wait.until(visibilityOf(passwordTextField)).sendKeys(password);
        clickEnterButton();
    }

    public void logout() {
        wait.until(visibilityOf(usernameLogout)).click();
    }
}
