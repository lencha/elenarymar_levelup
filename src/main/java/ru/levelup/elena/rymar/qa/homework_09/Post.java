package ru.levelup.elena.rymar.qa.homework_09;

import com.google.gson.annotations.SerializedName;

public class Post {
    private long id;
    @SerializedName("user_id")
    private long userId;
    private String title;
    private String body;

    public Post(long userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", user_id=" + userId +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }

    static class Builder {

        private long userId;
        private String title;
        private String body;

        public Builder() {
        }

        public Builder userId(long userId) {
            this.userId = userId;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder body(String body) {
            this.body = body;
            return this;
        }

        public Post build() {
            return new Post(userId, title, body);
        }
    }
}
