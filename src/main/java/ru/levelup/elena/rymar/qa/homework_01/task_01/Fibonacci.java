package ru.levelup.elena.rymar.qa.homework_01.task_01;

public class Fibonacci {
    public long fibonacci(int maxNumber) {
        long previousNumber = 0;
        long nextNumber = 1;
        if (maxNumber <= 199){
        for (int i = 1; i <= maxNumber; ++i)
        {
            long sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
        }
        System.out.println(previousNumber);
        return previousNumber;
        }else System.out.println("Введенное значение не может быть больше 199. Вычисление не возможно");
        return previousNumber;
    }
}
