package ru.levelup.elena.rymar.qa.homework_07.task_01;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class InboxPage {
    public static SelenideElement userEmail = $(By.id("PH_user-email"));
    public static SelenideElement createEmail = $("[title='Написать письмо']");

    @Step("Create new letter")
    public InboxPage createNewLetter() {
        createEmail.click();
        return this;
    }

    public static SelenideElement to = $("[data-type='to'] input");
    public static SelenideElement subject = $("input[name='Subject']");
    public static SelenideElement body = $("[role='textbox']");
    public static SelenideElement noMails = $(By.className("octopus__title"));

    public List<SelenideElement> mailSendAddressList = Collections.singletonList($(By.className("ll-crpt")));
    public List<SelenideElement> mailSubjectList = Collections.singletonList($(By.className("llc__subject")));
    public List<SelenideElement> mailTextList = Collections.singletonList($(By.className("ll-sp__normal")));

    public SelenideElement mailSubject = $(By.className("thread__subject"));
    public SelenideElement mailText = $(By.className("letter__body"));

    public static SelenideElement saveButton = $("[title='Сохранить']");
    public static SelenideElement closeButton = $("[title^='Закрыть']");
    public static SelenideElement sendButton = $x("//*[@title='Отправить']");
    public static SelenideElement deleteButton = $x("//*[@title='Удалить']");

    public static SelenideElement draftFolderButton = $("[title^='Черновики']");
    public static SelenideElement sendFolderButton = $("[title^='Отправленные']");
    public static SelenideElement deleteFolderButton = $x("//a[contains(@href,\"/trash/\")]");
    public static SelenideElement testFolderButton = $x("//div[contains(text(), 'Тест')]");

    @Step("Open letter")
    public void openMailByListIndex(int index) {
        mailSendAddressList.get(index).click();
    }

    @Step("Check MailText")
    public String getMailTextByListIndex(int index) {
        return mailTextList.get(index).getText();
    }

    @Step("Check MailSendAddress")
    public String getMailSendAddressByListIndex(int index) {
        return mailSendAddressList.get(index).getText();
    }

    @Step("Check MailSubject")
    public String getMailSubjectsByListIndex(int index) {
        return mailSubjectList.get(index).getText();
    }

    public String getMailSubject() {
        return mailSubject.getText();
    }

    public String getMailBody() {
        return mailText.getText();
    }

    @Step("Logout")
    public InboxPage clickLogoutButton() {
        $(By.id("PH_logoutLink")).click();
        return this;
    }
}
