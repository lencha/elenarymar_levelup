package ru.levelup.elena.rymar.qa.homework_09;

import com.google.gson.annotations.SerializedName;

public class CommentDetailsResponse {
    @SerializedName("_meta")
    private Meta meta;
    private Comment result;

    public CommentDetailsResponse(Meta meta, Comment result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public Comment getResult() {
        return result;
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public String toString() {
        return "CommentDetailsResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }

    static class Builder {

        private Meta meta;
        private Comment result;

        public Builder() {
        }

        public Builder meta(Meta meta) {
            this.meta = meta;
            return this;
        }

        public Builder result(Comment result) {
            this.result = result;
            return this;
        }

        public CommentDetailsResponse build() {
            return new CommentDetailsResponse(meta, result);
        }
    }
}
