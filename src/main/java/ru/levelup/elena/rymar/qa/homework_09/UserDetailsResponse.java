package ru.levelup.elena.rymar.qa.homework_09;

import com.google.gson.annotations.SerializedName;

public class UserDetailsResponse {
    @SerializedName("_meta")
    private Meta meta;
    private User result;

    public UserDetailsResponse(Meta meta, User result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public User getResult() {
        return result;
    }

    public static Builder builder(){
        return new Builder();
    }
    @Override
    public String toString() {
        return "UserDetailsResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }


    static class Builder {

        private Meta meta;
        private User result;

        public Builder() {
        }

        public Builder meta(Meta meta) {
            this.meta = meta;
            return this;
        }

        public Builder result(User result) {
            this.result = result;
            return this;
        }

        public UserDetailsResponse build() {
            return new UserDetailsResponse(meta, result);
        }
    }
}
