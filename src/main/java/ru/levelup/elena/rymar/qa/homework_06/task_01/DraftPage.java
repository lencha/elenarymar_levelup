package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class DraftPage extends BasePage{

    @FindBy(css = "[title^='Черновики']")
    private WebElement draftMailsButton;

    @FindBy(className = "octopus__title")
    private WebElement withoutDraftMails;

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public void pressDraftButton(){
        wait.until(ExpectedConditions.elementToBeClickable(draftMailsButton)).click();
    }

    public void openMailWindowByListIndex(int index) {
        wait.until(ExpectedConditions.visibilityOfAllElements(mailSendAddressList));
        mailSendAddressList.get(index).click();
    }

    public String noDraftMails() {
        return wait.until(visibilityOf(withoutDraftMails)).getText();
    }

}
