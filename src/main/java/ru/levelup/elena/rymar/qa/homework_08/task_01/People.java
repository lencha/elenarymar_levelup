package ru.levelup.elena.rymar.qa.homework_08.task_01;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class People {
    private long id;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    private String gender;
    private String dob;
    private String email;
    private String phone;
    private String website;
    private String address;
    private String status;

    public People(String firstName, String lastName, String gender, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dob = dob;
        this.email = email;
        this.phone = phone;
        this.website = website;
        this.address = address;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
/*        if (gender.equals("female")){
        return gender;
        }
        else if (gender.equals("male")){*/
        return gender;
        //}
    }

    public String getDob() {
        return dob;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public String getAddress() {
        return address;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        People people = (People) o;
        return id == people.id &&
                Objects.equals(firstName, people.firstName) &&
                Objects.equals(lastName, people.lastName) &&
                Objects.equals(gender, people.gender) &&
                Objects.equals(dob, people.dob) &&
                Objects.equals(email, people.email) &&
                Objects.equals(phone, people.phone) &&
                Objects.equals(website, people.website) &&
                Objects.equals(address, people.address) &&
                Objects.equals(status, people.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, gender, dob, email, phone, website, address, status);
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", dob=" + dob +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", website='" + website + '\'' +
                ", address='" + address + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
