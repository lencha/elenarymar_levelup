package ru.levelup.elena.rymar.qa.homework_09;

import java.util.Objects;

public class Meta {
    private boolean success;
    private String code;
    private String message;

    public Meta(boolean success, String code) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static Builder builder(){
        return new Builder();
    }

    @Override
    public String toString() {
        return "Meta{" +
                "success=" + success +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    static class Builder {

        private boolean success;
        private String code;

        public Builder() {
        }

        public Builder success(boolean success) {
            this.success = success;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Meta build() {
            return new Meta(success, code);
        }
    }
}
