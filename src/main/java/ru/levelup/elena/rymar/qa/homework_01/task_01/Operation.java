package ru.levelup.elena.rymar.qa.homework_01.task_01;

import java.util.Scanner;

public class Operation {

    Scanner scanner = new Scanner(System.in);

    public Operation() {
        this.scanner = scanner;
    }

    public char getOperation(){
        System.out.println("Введите операцию:");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("Вы допустили ошибку при вводе операции. Попробуйте еще раз.");
            scanner.next();    //рекурсия
            operation = getOperation();
        }
        return operation;
    }
}
