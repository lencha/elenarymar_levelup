package ru.levelup.elena.rymar.qa.homework_08.task_01;

import com.google.gson.annotations.SerializedName;

public class CommentDetailsResponse {
    @SerializedName("_meta")
    private Meta meta;
    private Comment result;

    public CommentDetailsResponse(Meta meta, Comment result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public Comment getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "CommentDetailsResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }
}
