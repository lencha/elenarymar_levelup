package ru.levelup.elena.rymar.qa.homework_01.task_01;

/*
 * Calculator
 *
 * v0.3
 *
 * Elena Rymar
 */

public class Calculator {

    private static final String MESSAGE = String.format("%s%n","Для начала работы с калькулятором вам необходимо ввести 2 целых числа. Поддерживаются следущие операции:");
    private static final String OPERATION_MESSAGE = String.format("%s%n%s%n%s%n%s%n%s%n%s%n", "1) сложение: '+'", "2) вычитание: '-'", "3) умножение: '*'", "4) возведение в степень: '^'", "5) вычисление факториала числа: '!'", "6) вычисление заданного числа Фибоначчи: 'f'");

    public static void main(String[] args) {
        Calculator app = new Calculator();
        app.startApplication();
    }

    public void startApplication() {

        //выполнение приложения
        System.out.println(MESSAGE + OPERATION_MESSAGE);
        Value a = new Value();
        int val1 = a.getIntValue();
        int val2 = a.getIntValue();
        Operation o = new Operation();
        char operation = o.getOperation();
        Calculation app = new Calculation();
        double result = app.calc(val1, val2, operation);
        System.out.println("Результат операции: " + result);
    }
}
