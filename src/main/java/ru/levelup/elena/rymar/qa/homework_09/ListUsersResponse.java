package ru.levelup.elena.rymar.qa.homework_09;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListUsersResponse {
    @SerializedName("_meta")
    private Meta meta;
    @SerializedName("result")
    private List<User> people;

    public ListUsersResponse(Meta meta, List<User> people) {
        this.meta = meta;
        this.people = people;
    }

    public Meta getMeta() {
        return meta;
    }

    public List<User> getPeople() {
        return people;
    }

    @Override
    public String toString() {
        return "ListUsersResponse{" +
                "_meta=" + meta +
                ", results=" + people +
                '}';
    }
}
