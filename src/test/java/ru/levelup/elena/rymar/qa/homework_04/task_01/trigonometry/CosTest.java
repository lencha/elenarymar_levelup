package ru.levelup.elena.rymar.qa.homework_04.task_01.trigonometry;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class CosTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Косинус:");
    }

    @Test(testName = "Cosinus",
            dataProviderClass = TestDataProvider.class, dataProvider = "CosDoubleDataProvider")
    public void cosDoubleTest(double a, double expectation){
        System.out.println("cos " + a + " = " + expectation);
        double result = calculator.cos(a);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}
