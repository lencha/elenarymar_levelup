package ru.levelup.elena.rymar.qa.homework_05;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected static final String MAIL_URL = "https://mail.ru";
    protected static final String MAIL_LOGIN = "testovyy.testik@bk.ru";
    protected static final String MAIL_PASSWORD = "26)w2V-;[jW&J98";
    protected static final String MAIL_SEND_ADDRESS = "testovyy.testik@bk.ru";
    protected static final String MAIL_SEND_NAME = "Тестик Тестовый";
    protected static final String MAIL_SUBJECT = "my First email to myself";
    protected static final String MAIL_TEXT = "Text bla-bla-bla";
    protected static final String MAIL_TESTSUBJECT = "Тест";

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(MAIL_URL);
        wait = new WebDriverWait(driver, 15);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
