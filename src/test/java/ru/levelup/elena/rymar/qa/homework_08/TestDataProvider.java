package ru.levelup.elena.rymar.qa.homework_08;

import org.testng.annotations.DataProvider;
import ru.levelup.elena.rymar.qa.homework_08.task_01.*;

public class TestDataProvider extends BaseTest{

    @DataProvider
    public Object[][] paramFirstNameDataProvider() {
        return new Object[][] {
                {"first_name", "Leonora"},
                {"first_name", "john"},
                {"first_name", "Afanasiy"}
        };
    }

    @DataProvider
    public Object[][] userDataProvider() {
        return new Object[][] {
                {new People("Leo", "Bernie","male","utro6012@example.com"),
                    new UserDetailsResponse(new Meta(true, CREATED_CODE),
                        new People("Leo", "Bernie","male","utro6012@example.com"))}
        };
    }

    @DataProvider
    public Object[][] paramIdDataProvider() {
        return new Object[][] {
                {"id", 123L},
                {"id", 11903},
                {"id", 11904},
                {"id", 74183L}
        };
    }

    @DataProvider
    public Object[][] userByIdProvider() {
        return new Object[][] {
                {"id", 74183, new People("Alesya", "A","female","utro601@example.com")}
        };
    }

    @DataProvider
    public Object[][] delIdDataProvider() {
        return new Object[][] {
                {"id", 123},
                {"id", 11904L},
                {"id", 11903}
        };
    }

    @DataProvider
    public Object[][] pageDataProvider() {
        return new Object[][] {
                {"page", 0},
                {"page", 5},
                {"page", 97},
                {"page", 999}
        };
    }

    @DataProvider
    public Object[][] paramTitleDataProvider() {
        return new Object[][] {
                {"title", "Voluptates optio atque"},
                {"title", "Id sint sunt dolores quo"}
        };
    }

    @DataProvider
    public Object[][] postDataProvider() {
        return new Object[][] {
                {new Post(74814,"maleldffv","body vkn;fveqrnernreb"),
                        new PostDetailsResponse(new Meta(true, CREATED_CODE),
                                new Post(74814,"maleldffv","body vkn;fveqrnernreb"))}
        };
    }

    @DataProvider
    public Object[][] paramPostIdDataProvider() {
        return new Object[][] {
                {"id", 123L},
                {"id", 6714},
                {"id", 6173L}
        };
    }

    @DataProvider
    public Object[][] postByIdProvider() {
        return new Object[][] {
                {"id", 15473L , new Post(74814,"maleldffv","body vkn;fveqrnernreb")}
        };
    }

    @DataProvider
    public Object[][] delPostIdDataProvider() {
        return new Object[][] {
                {"id", 123},
                {"id", 6714L}
        };
    }

    @DataProvider
    public Object[][] paramNameDataProvider() {
        return new Object[][] {
                {"name", "John"},
                {"name", "Leonora"},
                {"name", "Montana"},
                {"name", "Afanasiy"}
        };
    }

    @DataProvider
    public Object[][] commentDataProvider() {
        return new Object[][] {
                {new Comment(7187, "Leo","utro6012@example.com","fbslmfbsgbsbsrb"),
                        new CommentDetailsResponse(new Meta(true, CREATED_CODE),
                                new Comment(7187, "Leo","utro6012@example.com","fbslmfbsgbsbsrb"))}
        };
    }

    @DataProvider
    public Object[][] paramCommentIdDataProvider() {
        return new Object[][] {
                {"id",5467},
                {"id",123L}
        };
    }

    @DataProvider
    public Object[][] commentByIdProvider() {
        return new Object[][] {
                {"id", 13176L, new Comment(7187, "Leo","utro6012@example.com","fbslmfbsgbsbsrb")}
        };
    }

    @DataProvider
    public Object[][] delCommentByIdDataProvider() {
        return new Object[][] {
                {"id", 5502L},
                {"id", 13176L},
                {"id", 123}
        };
    }
}
