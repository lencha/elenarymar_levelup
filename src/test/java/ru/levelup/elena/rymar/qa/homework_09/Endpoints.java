package ru.levelup.elena.rymar.qa.homework_09;

public final class Endpoints {
    public static final String BASE_ENDPOINT = "https://gorest.co.in/public-api";
    public static final String GET_USER_BY_ID_ENDPONIT = "/users/{id}";
    public static final String USERS_ENDPOINT = "/users";
    public static final String POSTS_ENDPOINT = "/posts";
    public static final String GET_POST_BY_ID_ENDPONIT = "/posts/{id}";
    public static final String COMMENTS_ENDPOINT = "/comments";
    public static final String GET_COMMENT_BY_ID_ENDPONIT = "/comments/{id}";

    private Endpoints(){
    }
}

