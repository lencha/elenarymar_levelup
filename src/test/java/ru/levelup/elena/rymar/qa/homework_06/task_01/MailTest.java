package ru.levelup.elena.rymar.qa.homework_06.task_01;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MailTest extends BaseTest{

    @Test
    public void openMailTest() {
        HomePage homePage = new HomePage(driver);
        homePage.open(MAIL_URL);

        //       1. Login
        homePage.login(MAIL_LOGIN,MAIL_PASSWORD);

        //       2. Verify success entrance
        InboxPage inbox = new InboxPage(driver);
        assertEquals(inbox.getTextMenu(),"Входящие");

        //       3. Create new letter
        LetterPage letterPage = new LetterPage(driver);
        letterPage.createNewLetter();
        letterPage.setAddress(MAIL_SEND_ADDRESS);
        letterPage.setSubject(MAIL_SUBJECT);
        letterPage.setBody(MAIL_TEXT);

        //       4. Save draft letter
        letterPage.pressSaveDraft();
        letterPage.pressCloseDraft();

        //       5. Open draft folder
        DraftPage draftPage = new DraftPage(driver);
        draftPage.pressDraftButton();

        //        6. Verify draft letter in draft folder
        assertEquals(MAIL_SEND_ADDRESS, draftPage.getMailSendAddress(0));
        assertEquals(MAIL_SUBJECT, draftPage.getMailSubject(0));
        assertEquals(MAIL_TEXT, inbox.getMailBodyTextContains(MAIL_TEXT));
        draftPage.openMailWindowByListIndex(0);

        //        7. Send letter
        letterPage.pressSendDraft();
        letterPage.pressCloseDraft();

        //         8. Verify draft letter is not in draft folder
        assertEquals("У вас нет незаконченных\nили неотправленных писем", draftPage.noDraftMails());

        //         9. Verify letter in send folder
        SendPage sendPage = new SendPage(driver);
        sendPage.pressSendFolderButton();
        assertEquals(MAIL_SEND_ADDRESS, sendPage.getMailSendAddress(0));
        assertEquals("Self: "+MAIL_SUBJECT, sendPage.getMailSubject(0));
        assertEquals(MAIL_TEXT, sendPage.getMailText(0));

        //         10. Logout
        homePage.logout();
    }

}
