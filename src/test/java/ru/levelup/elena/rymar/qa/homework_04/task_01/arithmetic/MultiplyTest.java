package ru.levelup.elena.rymar.qa.homework_04.task_01.arithmetic;

import org.testng.Assert;
import org.testng.annotations.*;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class MultiplyTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Умножение:");
    }

    @Test(testName = "DoubleMultiply",
            dataProviderClass = TestDataProvider.class, dataProvider = "MulDoubleDataProvider")
    public void multiplyDoubleTest(double a, double b, double expected) {
        System.out.println(a + " * " + b + " = " + expected);
        double result = calculator.mult(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test(testName = "LongMultiply",
            dataProviderClass = TestDataProvider.class, dataProvider = "MulLongDataProvider")
    public void multiplyLongTest(double a, double b, double expected) {
        System.out.println(a + " * " + b + " = " + expected);
        double result = calculator.mult(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}