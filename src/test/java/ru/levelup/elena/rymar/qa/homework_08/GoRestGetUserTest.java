package ru.levelup.elena.rymar.qa.homework_08;

import io.restassured.http.ContentType;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_08.task_01.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GoRestGetUserTest extends BaseTest{

    @Test
    @Description("GET list all users")
    public void getUserRequestListTest() {
        ListUsersResponse usersResponse = given()
                .spec(rqSpec)
                .when()
                .get(Endpoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class);

        System.out.println(usersResponse);
        assertThat(usersResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(usersResponse.getPeople().isEmpty(), is(false));
        assertThat(usersResponse.getPeople(), is(notNullValue()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "paramFirstNameDataProvider")
    @Description("GET list all users with first name = john")
    public void getUserByFirstNameTest(String paramName, String paramValue) {
        ListUsersResponse usersResponse =
                given()
                .spec(rqSpec)
                .param(paramName, paramValue)
                .when()
                .get(Endpoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class);

        System.out.println(usersResponse);
        assertThat(usersResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(usersResponse.getPeople().isEmpty(), is(false));
        assertThat(usersResponse.getPeople(), is(notNullValue()));
        assert(usersResponse.getPeople().stream().filter(
                name ->name.getFirstName().contains(paramValue)).findFirst().isPresent());
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "userDataProvider")
    @Description("POST Create a new user")
    public void createNewPostTest(People request, UserDetailsResponse response) {
        UserDetailsResponse usersResponse =
                given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(Endpoints.USERS_ENDPOINT)
                .as(UserDetailsResponse.class);

        System.out.println(usersResponse);
        assertThat(usersResponse.getResult().getFirstName(), equalTo(request.getFirstName()));
        assertThat(usersResponse.getResult().getLastName(), equalTo(request.getLastName()));
        assertThat(usersResponse.getResult().getGender(), equalTo(request.getGender()));
        assertThat(usersResponse.getResult().getEmail(), equalTo(request.getEmail()));

        assertThat(usersResponse.getMeta(), samePropertyValuesAs(response.getMeta(),  "message"));
        assertThat(usersResponse.getResult(), samePropertyValuesAs(response.getResult(),  "id", "dob", "phone", "website", "address", "status"));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "paramIdDataProvider")
    @Description("GET return the details of the user 123")
    public void getUserByIdTest(String paramName, long paramValue) {
        UserDetailsResponse userResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .when()
                .get(Endpoints.GET_USER_BY_ID_ENDPONIT)
                .as(UserDetailsResponse.class);

        System.out.println(userResponse);
        assertThat(userResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(userResponse.getResult().getId(),equalTo(paramValue));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "userByIdProvider")
    @Description("PUT update the user 123")
    public void updateUserByIdTest(String paramName, long paramValue, People bodyUpdate) {
        UserDetailsResponse userResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .body(bodyUpdate)
                .when()
                .put(Endpoints.GET_USER_BY_ID_ENDPONIT)
                .as(UserDetailsResponse.class);

        System.out.println(userResponse);
        assertThat(userResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(userResponse.getResult().getFirstName(), equalTo(bodyUpdate.getFirstName()));
        assertThat(userResponse.getResult().getLastName(), equalTo(bodyUpdate.getLastName()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "delIdDataProvider")
    @Description("DELETE the user 123")
    public void deleteUserByIdTest(String paramName, long paramValue) {
        getUserByIdTest(paramName, paramValue);
        UserDetailsResponse userResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .when()
                .delete(Endpoints.GET_USER_BY_ID_ENDPONIT)
                .as(UserDetailsResponse.class);

        System.out.println(userResponse);
        assertThat(userResponse.getMeta().getCode(), equalTo(NOCONTENT_CODE));
        assertThat(userResponse.getResult(), is(nullValue()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider =  "pageDataProvider")
    @Description("GET list all users paged")
    public void getUserRequestListPageTest(String paramName, int paramValue) {
        ListUsersResponse usersResponse = given()
                .spec(rqSpec)
                .param(paramName,paramValue)
                .when()
                .get(Endpoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class);

        System.out.println(usersResponse);
        assertThat(usersResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(usersResponse.getPeople().isEmpty(), is(false));
        assertThat(usersResponse.getPeople(), is(notNullValue()));
    }
}
