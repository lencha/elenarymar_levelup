package ru.levelup.elena.rymar.qa.homework_07.task_02;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import ru.levelup.elena.rymar.qa.homework_07.task_01.ConfigurationSelenideTest;
import ru.levelup.elena.rymar.qa.homework_07.task_01.InboxPage;
import ru.levelup.elena.rymar.qa.homework_07.task_01.MailRuPage;

import static ru.levelup.elena.rymar.qa.homework_07.task_01.InboxPage.*;
import static com.codeborne.selenide.Condition.*;

public class MailTest2 extends ConfigurationSelenideTest {

    @Test
    @Story("HW-7: Test folder test")
    public void openMailTest2() {

        //       1. Login
        MailRuPage mailRuPage = new MailRuPage().open();
        mailRuPage.login(MAIL_LOGIN, MAIL_PASSWORD);

        //       2. Verify success entrance
        SoftAssert sa = new SoftAssert();
        InboxPage inboxPage = new InboxPage();
        sa.assertTrue(userEmail.waitUntil(visible, 5000).isDisplayed(), "User email");
        sa.assertEquals(userEmail.getText(), MAIL_SEND_ADDRESS);

        //       3. Create new letter
        inboxPage.createNewLetter();

        to.sendKeys(MAIL_SEND_ADDRESS);
        subject.sendKeys(MAIL_TESTSUBJECT);
        body.sendKeys(MAIL_TEXT);

        //        4. Send letter
        sendButton.shouldBe(visible).click();
        closeButton.click();

        //         5. Check letter in send folder
        sendFolderButton.click();
        sa.assertEquals("Send: " + inboxPage.getMailSendAddressByListIndex(0), "Send: " + MAIL_SEND_ADDRESS);
        sa.assertEquals("Send: " + inboxPage.getMailSubjectsByListIndex(0), "Send: " + "Self: "+MAIL_TESTSUBJECT);
        sa.assertEquals("Send: " + inboxPage.getMailTextByListIndex(0), "Send: " + MAIL_TEXT);

        //         6. Open test folder
        testFolderButton.click();

        //         7. Verify letter in test folder
        sa.assertEquals("Test: " + inboxPage.getMailSendAddressByListIndex(0), "Test: " + MAIL_SEND_ADDRESS);
        sa.assertEquals("Test: " + inboxPage.getMailSubjectsByListIndex(0), "Test: " + "Self: "+ MAIL_TESTSUBJECT);
        sa.assertEquals("Test: " + inboxPage.getMailTextByListIndex(0),"Test: " + MAIL_TEXT);

        //         8. Logout
        inboxPage.clickLogoutButton();
        sa.assertFalse(userEmail.isDisplayed(),"logout");
        sa.assertAll();
    }
}
