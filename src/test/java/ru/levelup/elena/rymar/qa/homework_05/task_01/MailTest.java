package ru.levelup.elena.rymar.qa.homework_05.task_01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_05.BaseTest;

import static org.testng.Assert.assertEquals;

public class MailTest extends BaseTest {

    @Test
    public void openMailTest(){
        long startTime;
        long endTime;

        String title = driver.getTitle();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");

        //       1. Login
        WebElement userNameTextFieldByName = driver.findElement(By.name("login"));
        userNameTextFieldByName.sendKeys(MAIL_LOGIN);
        WebElement entrance = driver.findElement(By.xpath("//input[@class='o-control']"));
        entrance.click();
        WebElement passwordTextFieldById = wait.until(ExpectedConditions.elementToBeClickable(By.id("mailbox:password")));
        passwordTextFieldById.sendKeys(MAIL_PASSWORD);
        entrance.click();

        //       2. Verify success entrance
        WebElement menu = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("portal-menu-element__text")));
        assertEquals(menu.getText(),"Входящие");

        //       3. Create new letter
        Actions action = new Actions(driver);
        action.click(menu);
        action.sendKeys("n").perform();

        WebElement to = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input")));
        to.sendKeys(MAIL_SEND_ADDRESS);
        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='Subject']")));
        subject.sendKeys(MAIL_SUBJECT);
        WebElement body = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']")));
        body.sendKeys(MAIL_TEXT);

        //       4. Save draft letter
        WebElement saveDraftButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Сохранить']")));
        saveDraftButton.click();
        WebElement closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[type='button'][title='Закрыть']")));
        closeButton.click();

        //       5. Open draft folder
        WebElement draftMails = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Черновики']")));
        draftMails.click();
        wait.until(ExpectedConditions.titleIs("Черновики - Почта Mail.ru"));
        WebElement draftEMail =driver.findElements(By.xpath("//*[@class='llc__item llc__item_correspondent']")).get(0);

        //        6. Verify draft letter in draft folder
        assertEquals(MAIL_SEND_ADDRESS, draftEMail.getText());
        assertEquals(MAIL_SUBJECT, driver.findElements(By.className("llc__subject")).get(0).getText());
        assertEquals(MAIL_TEXT, driver.findElements(By.className("ll-sp__normal")).get(0).getText());
        draftEMail.click();

        //        7. Send letter
        WebElement sendButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Отправить']")));
        sendButton.click();
        closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Закрыть']")));
        closeButton.click();

        //         8. Verify draft letter is not in draft folder
        WebElement withoutDraftMails = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("octopus__title")));
        assertEquals("У вас нет незаконченных\nили неотправленных писем", withoutDraftMails.getText());

        //         9. Verify letter in send folder
        startTime = System.currentTimeMillis();
        WebElement sendFolderButton;
        try {
            sendFolderButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Отправленные']")));
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", "[title='Отправленные']", (endTime - startTime)));
        }
        sendFolderButton.click();

        startTime = System.currentTimeMillis();
        WebElement sentEMail;
        try {
            sentEMail =wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='llc__item llc__item_correspondent']")));
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", "//*[@class='llc__item llc__item_correspondent']", (endTime - startTime)));
        }
        assertEquals(MAIL_SEND_ADDRESS, sentEMail.getText());
        assertEquals("Self: "+MAIL_SUBJECT, driver.findElements(By.className("llc__subject")).get(0).getText());
        assertEquals(MAIL_TEXT, driver.findElements(By.className("ll-sp__normal")).get(0).getText());

        //         10. Logout
        WebElement logout = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_logoutLink")));
        logout.click();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");
    }

}
