package ru.levelup.elena.rymar.qa.homework_06.task_03;

import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_06.task_01.BaseTest;
import ru.levelup.elena.rymar.qa.homework_06.task_01.HomePage;
import ru.levelup.elena.rymar.qa.homework_06.task_01.InboxPage;
import ru.levelup.elena.rymar.qa.homework_06.task_01.LetterPage;

import static org.testng.Assert.assertEquals;

public class MailTest3 extends BaseTest {

    @Test
    public void openMailTest() {
        HomePage homePage = new HomePage(driver);
        homePage.open(MAIL_URL);

        //       1. Login
        homePage.login(MAIL_LOGIN,MAIL_PASSWORD);

        //       2. Verify success entrance
        InboxPage inbox = new InboxPage(driver);

        //       3. Create new letter
        LetterPage letterPage = new LetterPage(driver);
        letterPage.createNewLetter();
        letterPage.setAddress(MAIL_SEND_ADDRESS);
        letterPage.setSubject(MAIL_SUBJECT);
        letterPage.setBody(MAIL_TEXT);

        //        4. Send letter
        letterPage.pressSendDraft();
        letterPage.pressCloseDraft();

        //         5. Check letter in inbox folder
        assertEquals(MAIL_SEND_NAME, inbox.getMailSendAddress(0));

        //        6. Verify letter in inbox folder
        inbox.openMail(0);
        assertEquals(MAIL_TEXT, inbox.getMailText(0));
        assertEquals(MAIL_SEND_ADDRESS, inbox.getMailAddress());
        assertEquals(MAIL_SUBJECT, inbox.getMailSubject(0));

        //         7. Delete letter from Inbox folder
        DeletePage deletePage = new DeletePage(driver);
        deletePage.pressDeleteFolderButton();

        //         8. Check delete folder
        assertEquals(MAIL_SEND_NAME, deletePage.getMailSendAddress(0));
        assertEquals(MAIL_SUBJECT, deletePage.getMailSubject(0));
        assertEquals(MAIL_TEXT, deletePage.getMailText(0));

        //         9. Logout
        homePage.logout();
    }
}
