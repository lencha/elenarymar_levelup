package ru.levelup.elena.rymar.qa.homework_09;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;
import ru.levelup.elena.rymar.qa.homework_08.Endpoints;

import static io.restassured.RestAssured.oauth2;

public abstract class BaseTest {
    private static final String TOKEN = "Dfo5d09Y9CyX8nKwB_eCdI0CC-QGh8VcpmPi";

    RequestSpecification rqSpec;
    ResponseSpecification rsSpec;

    protected static final String SUCCESS_CODE = "200";
    protected static final String CREATED_CODE = "201";
    protected static final String NOCONTENT_CODE = "204";

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_ENDPOINT)
                .setAuth(oauth2(TOKEN))
                .log(LogDetail.ALL)
                .build();

        rsSpec = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }
}
