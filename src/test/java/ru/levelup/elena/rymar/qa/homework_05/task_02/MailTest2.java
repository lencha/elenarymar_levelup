package ru.levelup.elena.rymar.qa.homework_05.task_02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_05.BaseTest;

import static org.testng.Assert.assertEquals;

public class MailTest2 extends BaseTest {

    @Test
    public void openMailTest() {
        String title = driver.getTitle();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");

        //       1. Login
        WebElement userNameTextFieldByName = driver.findElement(By.name("login"));
        userNameTextFieldByName.sendKeys(MAIL_LOGIN);
        WebElement entrance = driver.findElement(By.xpath("//input[@class='o-control']"));
        entrance.click();
        WebElement passwordTextFieldById = wait.until(ExpectedConditions.elementToBeClickable(By.id("mailbox:password")));
        passwordTextFieldById.sendKeys(MAIL_PASSWORD);
        entrance.click();

        //       2. verify success entrance
        WebElement menu = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("portal-menu-element__text")));
        assertEquals(menu.getText(),"Входящие");

        //       3. create new letter
        Actions action = new Actions(driver);
        action.click(menu);
        action.sendKeys("n").perform();

        WebElement to = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input")));
        to.sendKeys(MAIL_SEND_ADDRESS);
        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='Subject']")));
        subject.sendKeys(MAIL_TESTSUBJECT);
        WebElement body = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']")));
        body.sendKeys(MAIL_TEXT);

        //        4. Send letter
        WebElement sendButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Отправить']")));
        sendButton.click();
        WebElement closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Закрыть']")));
        closeButton.click();

        //         5. Check letter in send folder
        action.moveByOffset(10, 10).perform();
        WebElement sendFolderButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Отправленные']")));
        sendFolderButton.click();

        action.moveByOffset(10, 10).perform();
        WebElement sentEMail =driver.findElements(By.xpath("//*[@class='llc__item llc__item_correspondent']")).get(0);
        assertEquals(MAIL_SEND_ADDRESS, sentEMail.getText());
        assertEquals("Self: "+MAIL_TESTSUBJECT, driver.findElements(By.xpath("//*[@class='llc__subject']")).get(0).getText());
        assertEquals(MAIL_TEXT, driver.findElements(By.className("ll-sp__normal")).get(0).getText());

        //         6. Open test folder
        WebElement allFolders = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Все папки']")));
        allFolders.click();
        WebElement testFolder = wait.until(ExpectedConditions.elementToBeClickable(By.className("sidebar__full")));
        wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(testFolder, By.xpath("//div[contains(text(), 'Тест')]")));
        testFolder.click();

        //         7. Verify letter in test folder
        WebElement testEMail =driver.findElements(By.className("ll-crpt")).get(0);
        assertEquals(MAIL_SEND_ADDRESS, testEMail.getText());
        assertEquals("Self: "+MAIL_TESTSUBJECT, driver.findElements(By.className("llc__subject")).get(0).getText());
        assertEquals(MAIL_TEXT, driver.findElements(By.className("ll-sp__normal")).get(0).getText());

        //         8. Logout
        WebElement logout = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_logoutLink")));
        logout.click();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");
    }

}
