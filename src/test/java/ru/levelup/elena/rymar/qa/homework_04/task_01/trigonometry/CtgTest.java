package ru.levelup.elena.rymar.qa.homework_04.task_01.trigonometry;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class CtgTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Котангенс:");
    }

    @Test(testName = "Ctg",
            dataProviderClass = TestDataProvider.class, dataProvider = "CtgDoubleDataProvider")
    public void ctgDoubleTest(double a, double expectation){
        System.out.println("ctan " + a + " = " + expectation);
        double result = calculator.ctg(a);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}
