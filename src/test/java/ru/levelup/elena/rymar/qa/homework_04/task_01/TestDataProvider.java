package ru.levelup.elena.rymar.qa.homework_04.task_01;

import org.testng.annotations.DataProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TestDataProvider {
    @DataProvider
    public Object[][] AddDoubleDataProvider(){
        return new Object[][]{
                {2.0, 6.0, 8.0},
                {2.3, 4.4, 6.7},
                {0, 0, 0},
                {-0.1, -0.2, -0.3},
                {-1.0, -5.0, -6.0}
        };
    }
    @DataProvider
    public Object[][] AddLongDataProvider(){
        return new Object[][]{
                {0, 3000000000L, 3000000000L},
                {20000000000L, 0, 20000000000L},
                {0, 0, 0},
                {-20, 10, -10},
                {-1L, -5L, -6L}
        };
    }
    @DataProvider
    public Object[][] SubDoubleDataProvider(){
        return new Object[][]{
                {6.0, 2.0, 4.0},
                {6.7, 4.4, 2.3},
                {0, 0, 0},
                {-0.3, -0.2, -0.1},
                {-6.0, -5.0, -1.0}
        };
    }
    @DataProvider
    public Object[][] SubLongDataProvider(){
        return new Object[][]{
                {3000000000L, 3000000000L, 0},
                {20000000000L, 0, 20000000000L},
                {0, 0, 0},
                {-30, -10, -20},
                {-6L, -2L, -4L}
        };
    }

    @DataProvider
    public Object[][] MulDoubleDataProvider(){
        return new Object[][]{
                {6.0, 2.0, 12.0},
                {6.7, 4.4, 29.48},
                {40, 0, 0},
                {-0.3, -0.2, 0.06},
                {-6.0, 5.0, -30.0},
                {12345679.0, 9.0, 111111111.0}
        };
    }
    @DataProvider
    public Object[][] MulLongDataProvider(){
        return new Object[][]{
                {3000000000L, 3L, 9000000000L},
                {20000000000L, 0, 0},
                {40L, 0, 0},
                {-30, -10, 300},
                {-6.0, 2.0, -12.0}
        };
    }

    @DataProvider
    public Object[][] DivDoubleDataProvider(){
        return new Object[][]{
                {12.0, 2.0, 6.0},
                {29.48, 4.4, 6.7},
                {40, 0, Double.POSITIVE_INFINITY},
                {-20, 0, Double.NEGATIVE_INFINITY},
                {0, 40, 0},
                {34, 1, 34},
                {-0.06, -0.2, 0.3},
                {-30.0, 5.0, -6.0},
                {1.11111111E8,9.0,12345679.0}
        };
    }
    @DataProvider
    public Object[][] DivLongDataProvider(){
        return new Object[][]{
                {9000000000L, 3L, 3000000000L},
                {20000000000L, 1, 20000000000L},
                {0, -10L, 0},
                {-300, -10, 30},
                {-12L, 2L, -6L}
        };
    }

    @DataProvider
    public Object[][] PowDoubleDataProvider(){
        return new Object[][]{
                {5.0, 3.0, 125.0},
                {1.1, 2.2, 1.21},
                {40, 0, 1},
                {0, 40, 0},
                {-2.0, 3, -8.0},
                {-2.0, -2.0, 0.25},
                {1.0, -7.0, 1.0}
        };
    }
    @DataProvider
    public Object[][] SqrtDoubleDataProvider(){
        return new Object[][]{
                {25.0, 5.0},
                {0, 0},
                {0.36, 0.6},
                {1.1, 1.0488088481701516},
                {-3, Double.NaN}
        };
    }
    @DataProvider
    public Object[][] TgDoubleDataProvider(){
        return new Object[][]{
                {0.5, 0.54630248984},
                {40, -1.11721493092},
                {0, 0},
                {-90, 1.99520041221}
        };
    }
    @DataProvider
    public Object[][] CtgDoubleDataProvider(){
        return new Object[][]{
                {1, 0.64209261593},
                {0.5, 1.83048772171},
                {180, 0.74699881441},
                {-90, 0.50120278338}
        };
    }
    @DataProvider
    public Object[][] CosDoubleDataProvider(){
        return new Object[][]{
                {60.0, -0.95241298041},
                {1, 0.45359612142},
                {-120, 0.81418097052},
                {0, 1}
        };
    }
    @DataProvider
    public Object[][] SinDoubleDataProvider(){
        return new Object[][]{
                {0.5, 0.4794255386},
                {1, 0.8414709848},
                {-120, -0.58061118421},
                {0, 0}
        };
    }
}
