package ru.levelup.elena.rymar.qa.homework_04.task_01.arithmetic;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class SubstractTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Вычитание:");
    }

    @Test(testName = "DoubleSubstract",
            dataProviderClass = TestDataProvider.class, dataProvider = "SubDoubleDataProvider")
    public void substractDoubleTest(double a, double b, double expectation){
        System.out.println(a+" - "+b+" = "+expectation);
        double result = calculator.sub(a, b);
        Assert.assertEquals(result, expectation, 0.0000001);
    }

    @Test(testName = "LongSubstract",
            dataProviderClass = TestDataProvider.class, dataProvider = "SubLongDataProvider")
    public void additionLongTest(long a, long b, long expectation){
        System.out.println(a+" - "+b+" = "+expectation);
        long result = calculator.sub(a, b);
        Assert.assertEquals(result, expectation, 0.0000001);
    }
}