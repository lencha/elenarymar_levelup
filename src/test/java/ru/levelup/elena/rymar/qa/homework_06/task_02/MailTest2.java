package ru.levelup.elena.rymar.qa.homework_06.task_02;

import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_06.task_01.*;

import static org.testng.Assert.assertEquals;

public class MailTest2 extends BaseTest {

        @Test
        public void openMailTest() {
            HomePage homePage = new HomePage(driver);
            homePage.open(MAIL_URL);

            //       1. Login
            homePage.login(MAIL_LOGIN,MAIL_PASSWORD);

            //       2. Verify success entrance
            InboxPage inbox = new InboxPage(driver);

            //       3. Create new letter
            LetterPage letterPage = new LetterPage(driver);
            letterPage.createNewLetter();
            letterPage.setAddress(MAIL_SEND_ADDRESS);
            letterPage.setSubject(MAIL_TESTSUBJECT);
            letterPage.setBody(MAIL_TEXT);

            //        4. Send letter
            letterPage.pressSendDraft();
            letterPage.pressCloseDraft();

            //         5. Check letter in send folder
            SendPage sendPage = new SendPage(driver);
            sendPage.pressSendFolderButton();
            assertEquals(MAIL_SEND_ADDRESS, sendPage.getMailSendAddress(0));
            assertEquals("Self: "+MAIL_TESTSUBJECT, sendPage.getMailSubject(0));
            assertEquals(MAIL_TEXT, sendPage.getMailText(0));

            //         6. Open test folder
            inbox.pressAllFolders();
            TestPage testPage = new TestPage(driver);
            testPage.pressTestFolderButton();

            //         7. Verify letter in test folder
            assertEquals(MAIL_SEND_ADDRESS, testPage.getMailSendAddress(0));
            assertEquals("Self: "+MAIL_TESTSUBJECT, testPage.getMailSubject(0));
            assertEquals(MAIL_TEXT, testPage.getMailText(0));

            //         8. Logout
            homePage.logout();
        }
}
