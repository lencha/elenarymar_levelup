package ru.levelup.elena.rymar.qa.homework_04.task_01.trigonometry;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class SinTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Синус:");
    }

    @Test(testName = "Sin",
            dataProviderClass = TestDataProvider.class, dataProvider = "SinDoubleDataProvider")
    public void sinDoubleTest(double a, double expectation){
        System.out.println("sin " + a + " = " + expectation);
        double result = calculator.sin(a);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}
