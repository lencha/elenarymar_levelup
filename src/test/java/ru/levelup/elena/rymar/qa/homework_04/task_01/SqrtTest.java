package ru.levelup.elena.rymar.qa.homework_04.task_01;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SqrtTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Квадратный корень:");
    }

    @Test(testName = "Square root",
            dataProviderClass = TestDataProvider.class, dataProvider = "SqrtDoubleDataProvider")
    public void sqrtDoubleTest(double a, double expectation){
        System.out.println("√ " + a + " = " + expectation);
        double result = calculator.sqrt(a);
        Assert.assertEquals(result, expectation, 0.0000001);
    }
}
