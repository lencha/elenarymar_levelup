package ru.levelup.elena.rymar.qa.homework_07.task_01;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class ConfigurationSelenideTest {

    protected static final String MAIL_LOGIN = "testovyy.testik@bk.ru";
    protected static final String MAIL_PASSWORD = "26)w2V-;[jW&J98";
    protected static final String MAIL_SEND_ADDRESS = "testovyy.testik@bk.ru";
    protected static final String MAIL_SEND_NAME = "Тестик Тестовый";
    protected static final String MAIL_SUBJECT = "my First email to myself";
    protected static final String MAIL_TESTSUBJECT = "Тест";
    protected static final String MAIL_TEXT = "Text bla-bla-bla-bla";
    protected static final String MESSAGE = "У вас нет незаконченных\nили неотправленных писем";

    @BeforeMethod
    public void setUp() {
        Configuration.browser = Browsers.CHROME;
        Configuration.timeout = 7500;
        Configuration.baseUrl = "https://mail.ru";
        Configuration.reportsFolder = "target/reports/tests";
        Configuration.startMaximized = true;
        Configuration.headless = true;
    }

    @AfterMethod
    public void tearDown() {
        Selenide.closeWindow();
    }

    @BeforeSuite
    public void beforeSuite() {
        SelenideLogger.addListener("AllureSelenide",
                new AllureSelenide().savePageSource(true).screenshots(true));

        System.out.println();
    }

}
