package ru.levelup.elena.rymar.qa.homework_04.task_01.arithmetic;

import org.testng.Assert;
import org.testng.annotations.*;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class AdditionTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Сложение:");
    }

    @Test(testName = "DoubleAddition",
            dataProviderClass = TestDataProvider.class, dataProvider = "AddDoubleDataProvider")
    public void additionDoubleTest(double a, double b, double expectation){
        System.out.println(a+" + "+b+" = "+expectation);
        double result = calculator.sum(a, b);
        Assert.assertEquals(result, expectation,0.0000001);
    }

    @Test(testName = "LongAddition",
            dataProviderClass = TestDataProvider.class, dataProvider = "AddLongDataProvider")
    public void additionLongTest(long a, long b, long expectation){
        System.out.println(a+" + "+b+" = "+expectation);
        long result = calculator.sum(a, b);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}