package ru.levelup.elena.rymar.qa.homework_08;

import io.restassured.http.ContentType;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_08.task_01.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GoRestGetPostTest extends BaseTest {

    @Test
    @Description("GET list all posts")
    public void getPostRequestListTest() {
        ListPostsResponse postsResponse = given()
                .spec(rqSpec)
                .when()
                .get(Endpoints.POSTS_ENDPOINT)
                .as(ListPostsResponse.class);

        System.out.println(postsResponse);
        assertThat(postsResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(postsResponse.getPost().isEmpty(), is(false));
        assertThat(postsResponse.getPost(), is(notNullValue()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "paramTitleDataProvider")
    @Description("GET list all posts with title contains = Voluptates optio atque")
    public void getPostByFirstNameTest(String paramName, String paramValue) {
        ListPostsResponse postsResponse =
                given()
                .spec(rqSpec)
                .param(paramName, paramValue)
                .when()
                .get(Endpoints.POSTS_ENDPOINT)
                .as(ListPostsResponse.class);

        System.out.println(postsResponse);
        assertThat(postsResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(postsResponse.getPost().isEmpty(), is(false));
        assertThat(postsResponse.getPost(), is(notNullValue()));
        assert(postsResponse.getPost().stream().filter(
                title ->title.getTitle().contains(paramValue)).findFirst().isPresent());
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "postDataProvider")
    @Description("POST Create a new post")
    public void createNewPostTest(Post request, PostDetailsResponse response) {
        PostDetailsResponse postResponse =
                given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(Endpoints.POSTS_ENDPOINT)
                .as(PostDetailsResponse.class);
        System.out.println(postResponse);
        assertThat(postResponse.getResult().getTitle(), equalTo(request.getTitle()));
        assertThat(postResponse.getMeta(), samePropertyValuesAs(response.getMeta(),  "message"));
        assertThat(postResponse.getResult(), samePropertyValuesAs(response.getResult(),  "id"));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "paramPostIdDataProvider")
    @Description("GET return the details of the post 123")
    public void getPostByIdTest(String paramName, long paramValue) {
        PostDetailsResponse postResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .when()
                .get(Endpoints.GET_POST_BY_ID_ENDPONIT)
                .as(PostDetailsResponse.class);

        System.out.println(postResponse);
        assertThat(postResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(postResponse.getResult().getId(), equalTo(paramValue));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "postByIdProvider")
    @Description("PUT update the post 123")
    public void updatePostByIdTest(String paramName, long paramValue, Post postUpdate) {
        PostDetailsResponse postResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .body(postUpdate)
                .when()
                .put(Endpoints.GET_POST_BY_ID_ENDPONIT)
                .as(PostDetailsResponse.class);

        System.out.println(postResponse);
        assertThat(postResponse.getMeta().getCode(),equalTo(SUCCESS_CODE));
        assertThat(postResponse.getResult().getTitle(),equalTo(postUpdate.getTitle()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "delPostIdDataProvider")
    @Description("DELETE the post 123")
    public void deletePostByIdTest(String paramName, long paramValue) {
        getPostByIdTest(paramName, paramValue);
        PostDetailsResponse postResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .when()
                .delete(Endpoints.GET_POST_BY_ID_ENDPONIT)
                .as(PostDetailsResponse.class);

        System.out.println(postResponse);
        assertThat(postResponse.getMeta().getCode(),equalTo(NOCONTENT_CODE));
        assertThat(postResponse.getResult(), is(nullValue()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider =  "pageDataProvider")
    @Description("GET list all posts paged")
    public void getPostRequestListPageTest(String paramName, int paramValue) {
        ListPostsResponse postsResponse = given()
                .spec(rqSpec)
                .param(paramName, paramValue)
                .when()
                .get(Endpoints.POSTS_ENDPOINT)
                .as(ListPostsResponse.class);

        System.out.println(postsResponse);
        assertThat(postsResponse.getMeta().getCode(),equalTo(SUCCESS_CODE));
        assertThat(postsResponse.getPost().isEmpty(), is(false));
        assertThat(postsResponse.getPost(), is(notNullValue()));
    }
}
