package ru.levelup.elena.rymar.qa.homework_09;

import io.restassured.http.ContentType;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GoRestGetCommentTest extends BaseTest {

    @Test
    @Description("GET list all comments")
    public void getCommentRequestListTest() {
        ListCommentsResponse commentsResponse = given()
                .spec(rqSpec)
                .when()
                .get(Endpoints.COMMENTS_ENDPOINT)
                .as(ListCommentsResponse.class);

        System.out.println(commentsResponse);
        assertThat(commentsResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(commentsResponse.getComment().isEmpty(), is(false));
        assertThat(commentsResponse.getComment(), is(notNullValue()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "paramNameDataProvider")
    @Description("GET list all comments with name = john")
    public void getCommentByNameTest(String paramName, String paramValue) {
        ListCommentsResponse commentsResponse =
                given()
                .spec(rqSpec)
                .param(paramName, paramValue)
                .when()
                .get(Endpoints.COMMENTS_ENDPOINT)
                .as(ListCommentsResponse.class);

        System.out.println(commentsResponse);
        assertThat(commentsResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(commentsResponse.getComment().isEmpty(), is(false));
        assertThat(commentsResponse.getComment(), is(notNullValue()));
        assert(commentsResponse.getComment().stream().filter(
                name ->name.getName().contains(paramValue)).findFirst().isPresent());
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "commentDataProvider")
    @Description("POST Create a new user")
    public void createNewPostTest(Comment request, CommentDetailsResponse response) {
        CommentDetailsResponse commentResponse =
                given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(Endpoints.COMMENTS_ENDPOINT)
                .as(CommentDetailsResponse.class);

        System.out.println(commentResponse);
        assertThat(commentResponse.getResult().getName(), equalTo(request.getName()));
        assertThat(commentResponse.getResult().getEmail(), equalTo(request.getEmail()));

        assertThat(commentResponse.getMeta(), samePropertyValuesAs(response.getMeta(),  "message"));
        assertThat(commentResponse.getResult(), samePropertyValuesAs(response.getResult(),  "id"));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "paramCommentIdDataProvider")
    @Description("GET return the details of the comment 123")
    public void getCommentByIdTest(String paramName, long paramValue) {
        CommentDetailsResponse commentResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .when()
                .get(Endpoints.GET_COMMENT_BY_ID_ENDPONIT)
                .as(CommentDetailsResponse.class);

        System.out.println(commentResponse);
        assertThat(commentResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(commentResponse.getResult().getId(),equalTo(paramValue));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "commentByIdProvider")
    @Description("PUT update the comment 123")
    public void updateCommentByIdTest(String paramName, long paramValue, Comment bodyUpdate) {
        CommentDetailsResponse commentResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .body(bodyUpdate)
                .when()
                .put(Endpoints.GET_COMMENT_BY_ID_ENDPONIT)
                .as(CommentDetailsResponse.class);

        System.out.println(commentResponse);
        assertThat(commentResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(commentResponse.getResult().getName(), equalTo(bodyUpdate.getName()));
        assertThat(commentResponse.getResult().getEmail(), equalTo(bodyUpdate.getEmail()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider = "delCommentByIdDataProvider")
    @Description("DELETE the comment 123")
    public void deleteCommentByIdTest(String paramName, long paramValue) {
        getCommentByIdTest(paramName, paramValue);
        CommentDetailsResponse commentResponse = given()
                .spec(rqSpec)
                .pathParam(paramName, paramValue)
                .when()
                .delete(Endpoints.GET_COMMENT_BY_ID_ENDPONIT)
                .as(CommentDetailsResponse.class);

        System.out.println(commentResponse);
        assertThat(commentResponse.getMeta().getCode(), equalTo(NOCONTENT_CODE));
        assertThat(commentResponse.getResult(), is(nullValue()));
    }

    @Test(dataProviderClass = TestDataProvider.class, dataProvider =  "pageDataProvider")
    @Description("GET list all comments paged")
    public void getCommentRequestListPageTest(String paramName, int paramValue) {
        ListCommentsResponse commentsResponse = given()
                .spec(rqSpec)
                .param(paramName,paramValue)
                .when()
                .get(Endpoints.COMMENTS_ENDPOINT)
                .as(ListCommentsResponse.class);

        System.out.println(commentsResponse);
        assertThat(commentsResponse.getMeta().getCode(), equalTo(SUCCESS_CODE));
        assertThat(commentsResponse.getComment().isEmpty(), is(false));
        assertThat(commentsResponse.getComment(), is(notNullValue()));
    }
}
