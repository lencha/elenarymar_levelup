package ru.levelup.elena.rymar.qa.homework_09;

import org.testng.annotations.DataProvider;

public class TestDataProvider extends BaseTest {

    @DataProvider
    public Object[][] paramFirstNameDataProvider() {
        return new Object[][]{
                {"first_name", "Leonora"},
                {"first_name", "john"},
                {"first_name", "Afanasiy"}
        };
    }

    @DataProvider
    public Object[][] userDataProvider() {
        return new Object[][]{
                {
                        User
                                .builder()
                                .firstName("Ostap")
                                .lastName("Bender")
                                .gender("male")
                                .email("asta@lal.vista")
                                .build(),
                        UserDetailsResponse
                                .builder()
                                .meta(
                                        Meta
                                                .builder()
                                                .success(true)
                                                .code(CREATED_CODE)
                                                .build()
                                )
                                .result(
                                        User
                                                .builder()
                                                .firstName("Ostap")
                                                .lastName("Bender")
                                                .gender("male")
                                                .email("asta@lal.vista")
                                                .build()
                                )
                                .build()
                }
        };
    }

    @DataProvider
    public Object[][] paramIdDataProvider() {
        return new Object[][]{
                {"id", 123L},
                {"id", 11903},
                {"id", 11904},
                {"id", 74183L}
        };
    }

    @DataProvider
    public Object[][] userByIdProvider() {
        return new Object[][]{
                {"id", 74183,
                        User
                                .builder()
                                .firstName("Alesya")
                                .lastName("A")
                                .gender("female")
                                .email("utro601@example.com")
                                .build()
                }
        };
    }

    @DataProvider
    public Object[][] delIdDataProvider() {
        return new Object[][]{
                {"id", 123},
                {"id", 11904L},
                {"id", 11903}
        };
    }

    @DataProvider
    public Object[][] pageDataProvider() {
        return new Object[][]{
                {"page", 0},
                {"page", 5},
                {"page", 97},
                {"page", 999}
        };
    }

    @DataProvider
    public Object[][] paramTitleDataProvider() {
        return new Object[][]{
                {"title", "Voluptates optio atque"},
                {"title", "Id sint sunt dolores quo"}
        };
    }

    @DataProvider
    public Object[][] postDataProvider() {
        return new Object[][]{
                {
                        Post
                                .builder()
                                .userId(82907)
                                .title("12 chairs")
                                .body("fvm[wrverbm[erqbdva")
                                .build(),
                        PostDetailsResponse
                                .builder()
                                .meta(
                                        Meta
                                                .builder()
                                                .success(true)
                                                .code(CREATED_CODE)
                                                .build()
                                )
                                .result(
                                        Post
                                                .builder()
                                                .userId(82907)
                                                .title("12 chairs")
                                                .body("fvm[wrverbm[erqbdva")
                                                .build()
                                )
                                .build()
                }
        };
    }

    @DataProvider
    public Object[][] paramPostIdDataProvider() {
        return new Object[][]{
                {"id", 123L},
                {"id", 6714},
                {"id", 6173L}
        };
    }

    @DataProvider
    public Object[][] postByIdProvider() {
        return new Object[][]{
                {"id", 15473L,
                        Post
                                .builder()
                                .userId(74814)
                                .title("maleldffv")
                                .body("body vkn;fveqrnernreb")
                                .build()
                }
        };
    }

    @DataProvider
    public Object[][] delPostIdDataProvider() {
        return new Object[][]{
                {"id", 123},
                {"id", 6714L}
        };
    }

    @DataProvider
    public Object[][] paramNameDataProvider() {
        return new Object[][]{
                {"name", "John"},
                {"name", "Leonora"},
                {"name", "Montana"},
                {"name", "Afanasiy"}
        };
    }

    @DataProvider
    public Object[][] commentDataProvider() {
        return new Object[][]{
                {
                        Comment
                                .builder()
                                .postId(16043)
                                .name("Ostap")
                                .email("asta@la.vista")
                                .body("advqe[vmv[emwerq")
                                .build(),
                        CommentDetailsResponse
                                .builder()
                                .meta(
                                        Meta
                                                .builder()
                                                .success(true)
                                                .code(CREATED_CODE)
                                                .build()
                                )
                                .result(
                                        Comment
                                                .builder()
                                                .postId(16043)
                                                .name("Ostap")
                                                .email("asta@la.vista")
                                                .body("advqe[vmv[emwerq")
                                                .build()
                                )
                                .build()
                }
        };
    }

    @DataProvider
    public Object[][] paramCommentIdDataProvider() {
        return new Object[][]{
                {"id", 5467},
                {"id", 123L}
        };
    }

    @DataProvider
    public Object[][] commentByIdProvider() {
        return new Object[][]{
                {"id", 13176L,
                        Comment
                                .builder()
                                .postId(7187)
                                .name("Leo")
                                .email("utro6012@example.com")
                                .body("fbslmfbsgbsbsrb")
                                .build()
                }
        };
    }

    @DataProvider
    public Object[][] delCommentByIdDataProvider() {
        return new Object[][]{
                {"id", 5502L},
                {"id", 13176L},
                {"id", 123}
        };
    }
}
