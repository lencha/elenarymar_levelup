package ru.levelup.elena.rymar.qa.homework_04.task_01.arithmetic;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class DivisionTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Деление:");
    }

    @Test(testName = "DoubleDivision",
            dataProviderClass = TestDataProvider.class, dataProvider = "DivDoubleDataProvider")
    public void divDoubleTest(double a, double b, double expectation){
        System.out.println(a+" / "+b+" = "+expectation);
        double result = calculator.div(a, b);
        Assert.assertEquals(result, expectation,0.0000001);
    }

    @Test(testName = "LongDivision",
            dataProviderClass = TestDataProvider.class, dataProvider = "DivLongDataProvider")
    public void divLongTest(long a, long b, long expectation){
        System.out.println(a+" / "+b+" = "+expectation);
        long result = calculator.div(a, b);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}