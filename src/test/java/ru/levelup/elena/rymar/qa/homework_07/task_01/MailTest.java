package ru.levelup.elena.rymar.qa.homework_07.task_01;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static ru.levelup.elena.rymar.qa.homework_07.task_01.InboxPage.*;
import static com.codeborne.selenide.Condition.*;

public class MailTest extends ConfigurationSelenideTest {

    @Test
    @Story("HW-7: Send letter Test")
    public void openMailTest() {

        //       1. Login
        MailRuPage mailRuPage = new MailRuPage().open();
        mailRuPage.login(MAIL_LOGIN, MAIL_PASSWORD);

        //       2. Verify success entrance
        SoftAssert sa = new SoftAssert();
        InboxPage inboxPage = new InboxPage();
        sa.assertTrue(userEmail.waitUntil(visible, 5000).isDisplayed(), "User email");
        sa.assertEquals(userEmail.getText(), MAIL_SEND_ADDRESS);

        //       3. Create new letter
        createEmail.shouldBe(visible);
        inboxPage.createNewLetter();

        to.sendKeys(MAIL_SEND_ADDRESS);
        subject.sendKeys(MAIL_SUBJECT);
        body.sendKeys(MAIL_TEXT);

        //       4. Save draft letter
        saveButton.shouldBe(visible).click();
        closeButton.click();

        //       5. Open draft folder
        draftFolderButton.click();

        //        6. Verify draft letter in draft folder
        sa.assertEquals("Draft: " + inboxPage.getMailSendAddressByListIndex(0), "Draft: " + MAIL_SEND_ADDRESS);
        sa.assertEquals("Draft: " + inboxPage.getMailSubjectsByListIndex(0), "Draft: " + MAIL_SUBJECT);
        sa.assertEquals("Draft: " + inboxPage.getMailTextByListIndex(0), "Draft: " + MAIL_TEXT);
        inboxPage.openMailByListIndex(0);

        //        7. Send letter
        sendButton.shouldBe(visible).click();
        closeButton.click();

        //         8. Verify draft letter is not in draft folder
        sa.assertEquals(noMails.getText(), MESSAGE);
        inboxPage.mailSendAddressList.get(0).shouldNot(exist);

        //         9. Verify letter in send folder
        sendFolderButton.click();
        sa.assertEquals("Send: " + inboxPage.getMailSendAddressByListIndex(0), "Send: " + MAIL_SEND_ADDRESS);
        sa.assertEquals("Send: " + inboxPage.getMailSubjectsByListIndex(0), "Send: " + "Self: " + MAIL_SUBJECT);
        sa.assertEquals("Send: " + inboxPage.getMailTextByListIndex(0), "Send: " + MAIL_TEXT);

        //         10. Logout
/*        inboxPage.clickLogoutButton();
        sa.assertFalse(userEmail.isDisplayed(), "logout");
        sa.assertAll();*/
    }

}
