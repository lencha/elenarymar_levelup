package ru.levelup.elena.rymar.qa.homework_07.task_03;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import ru.levelup.elena.rymar.qa.homework_07.task_01.ConfigurationSelenideTest;
import ru.levelup.elena.rymar.qa.homework_07.task_01.InboxPage;
import ru.levelup.elena.rymar.qa.homework_07.task_01.MailRuPage;

import static com.codeborne.selenide.Condition.visible;
import static ru.levelup.elena.rymar.qa.homework_07.task_01.InboxPage.*;

public class MailTest3 extends ConfigurationSelenideTest {

    @Test(testName = "Test3")
    @Story("HW-7: Send letter to myself")
    public void openMailTest3() {

        //       1. Login
        MailRuPage mailRuPage = new MailRuPage().open();
        mailRuPage.login(MAIL_LOGIN, MAIL_PASSWORD);

        //       2. Verify success entrance
        SoftAssert sa = new SoftAssert();
        InboxPage inboxPage = new InboxPage();
        sa.assertTrue(userEmail.waitUntil(visible, 5000).isDisplayed(), "User email");
        sa.assertEquals(userEmail.getText(), MAIL_SEND_ADDRESS);

        //       3. Create new letter
        inboxPage.createNewLetter();

        to.sendKeys(MAIL_SEND_ADDRESS);
        subject.sendKeys(MAIL_SUBJECT);
        body.sendKeys(MAIL_TEXT);

        //        4. Send letter
        sendButton.shouldBe(Condition.visible).click();
        closeButton.click();

        //         5. Check letter in inbox folder
        sa.assertEquals(inboxPage.getMailSendAddressByListIndex(0),MAIL_SEND_NAME);
        inboxPage.openMailByListIndex(0);

        //        6. Verify letter in inbox folder
        sa.assertEquals("Inbox: " + inboxPage.getMailSendAddressByListIndex(0), "Inbox: " + MAIL_SEND_NAME);
        sa.assertEquals("Inbox: " + inboxPage.getMailSubject(), "Inbox: " + MAIL_SUBJECT);
        sa.assertEquals("Inbox: " + inboxPage.getMailBody(), "Inbox: " + MAIL_TEXT + "\n  ");

        //         7. Delete letter from Inbox folder
        deleteButton.click();

        //         8. Check delete folder
        deleteFolderButton.click();
        sa.assertEquals("Delete: " + inboxPage.getMailSendAddressByListIndex(0), "Delete: " + MAIL_SEND_NAME);
        sa.assertEquals("Delete: " + inboxPage.getMailSubjectsByListIndex(0), "Delete: " + MAIL_SUBJECT);
        sa.assertEquals("Delete: " + inboxPage.getMailTextByListIndex(0), "Delete: " + MAIL_TEXT);

        //         9. Logout
        inboxPage.clickLogoutButton();
        sa.assertFalse(userEmail.isDisplayed(),"logout");
        sa.assertAll();
    }
}
