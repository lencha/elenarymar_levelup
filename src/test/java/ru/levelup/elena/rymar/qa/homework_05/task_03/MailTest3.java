package ru.levelup.elena.rymar.qa.homework_05.task_03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_05.BaseTest;

import static org.testng.Assert.assertEquals;

public class MailTest3 extends BaseTest {

    @Test
    public void openMailTest() {

        String title = driver.getTitle();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");

        //       1. Login
        WebElement userNameTextFieldByName = driver.findElement(By.name("login"));
        userNameTextFieldByName.sendKeys(MAIL_LOGIN);
        WebElement entrance = driver.findElement(By.xpath("//input[@class='o-control']"));
        entrance.click();
        WebElement passwordTextFieldById = wait.until(ExpectedConditions.elementToBeClickable(By.id("mailbox:password")));
        passwordTextFieldById.sendKeys(MAIL_PASSWORD);
        entrance.click();

        //       2. Verify success entrance
        WebElement menu = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("portal-menu-element__text")));
        assertEquals(menu.getText(),"Входящие");

        //       3. Create new mail
        Actions action = new Actions(driver);
        action.click(menu);
        action.sendKeys("n").perform();

        WebElement to = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input")));
        to.sendKeys(MAIL_SEND_ADDRESS);
        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='Subject']")));
        subject.sendKeys(MAIL_SUBJECT);
        WebElement body = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']")));
        body.sendKeys(MAIL_TEXT);

        //        4. Send letter
        WebElement sendButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Отправить']")));
        sendButton.click();
        WebElement closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Закрыть']")));
        closeButton.click();

        //         5. Check letter in inbox folder
        assertEquals(menu.getText(),"Входящие");
        WebElement mailSendName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ll-crpt")));
        assertEquals(MAIL_SEND_NAME, mailSendName.getText());

        //        6. Verify letter in inbox folder
        WebElement draftMailBody = driver.findElement(By.xpath("//*[text()[contains(., '" + MAIL_TEXT + "')]]"));
        draftMailBody.click();
        assertEquals(MAIL_TEXT, draftMailBody.getText());
        String mailAddress = wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.xpath("//*[@class='letter__author']/span"))).getAttribute("title");
        assertEquals(MAIL_SEND_ADDRESS, mailAddress);
        String emailSubject = wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.xpath("//*[@class='thread__header']//h2"))).getText();
        assertEquals(MAIL_SUBJECT, emailSubject);

        //         7. Delete letter from Inbox folder
        WebElement deleteButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Удалить']")));
        deleteButton.click();

        //         8. Check delete folder
        WebElement deleteFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@href, \"/trash/\")]")));
        deleteFolder.click();
        WebElement mailDeleteName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ll-crpt")));
        assertEquals(MAIL_SEND_NAME, mailDeleteName.getText());
        WebElement mailDeleteSubject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("llc__subject")));
        assertEquals(MAIL_SUBJECT, mailDeleteSubject.getText());
        WebElement mailDeleteText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("llc__snippet")));
        assertEquals(MAIL_TEXT, mailDeleteText.getText());

        //         9. Logout
        WebElement logout = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_logoutLink")));
        logout.click();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");
    }

}
