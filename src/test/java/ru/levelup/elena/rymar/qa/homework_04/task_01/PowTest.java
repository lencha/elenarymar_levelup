package ru.levelup.elena.rymar.qa.homework_04.task_01;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PowTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("возведение в степень:");
    }

    @Test(testName = "Exponentiation",
            dataProviderClass = TestDataProvider.class, dataProvider = "PowDoubleDataProvider")
    public void powDoubleTest(double a, double b, double expectation){
        System.out.println(a + " ^ " + b + " = " + expectation);
        double result = calculator.pow(a, b);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}
