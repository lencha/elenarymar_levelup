package ru.levelup.elena.rymar.qa.homework_04.task_01;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.*;

public abstract class BaseCalculatorTest {
    protected Calculator calculator;
    @BeforeMethod
    public void setUpBeforeMethod() {
        calculator = new Calculator();
    }
    @AfterMethod
    public void tearDownAfterMethod() {
        calculator = null;
    }
    @AfterTest
    public void tearDownAfterTest() {
        System.out.println();
    }
}
