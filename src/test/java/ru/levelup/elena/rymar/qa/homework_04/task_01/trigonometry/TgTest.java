package ru.levelup.elena.rymar.qa.homework_04.task_01.trigonometry;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.levelup.elena.rymar.qa.homework_04.task_01.BaseCalculatorTest;
import ru.levelup.elena.rymar.qa.homework_04.task_01.TestDataProvider;

public class TgTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Тангенс:");
    }

    @Test(testName = "Tg",
            dataProviderClass = TestDataProvider.class, dataProvider = "TgDoubleDataProvider")
    public void tgDoubleTest(double a, double expectation){
        System.out.println("tan " + a + " = " + expectation);
        double result = calculator.tg(a);
        Assert.assertEquals(result, expectation,0.0000001);
    }
}
